#!/usr/bin/env bash

# finding our directory
full_path=$(realpath $0)
# echo $full_path
dir_path=$(dirname $full_path)
# echo $dir_path
cd "${dir_path}"

# build and run our container, editing its own files
sudo docker build -t a/dc-linux -f Dockerfile . && \
  sudo docker run -it a/dc-linux
