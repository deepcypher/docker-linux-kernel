Dockerised Linux Kernel Compilation
===================================

I wanted a pure clean environment with which to build kernels easily.
The original intention is to fascilitate easily swapping configurations to build the kernel for any target desired. Being dockerised also potentially allows for some neat cross compilation tricks with QEMU to build many containers for many architectures, so everyone everywhere can start the compilation of their own kernels in seconds with relativeley little knowledge.
However this also functions as a learning tool for myself in particular to come to grips with linux compilation itself. Hey who knows, maybe it will actually help someone else learn too.
