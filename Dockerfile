FROM alpine:edge

ARG USERNAME="archie"
ARG NEOVIM_CONFIG_DIR=".config/nvim"
ARG PLAYGROUND_DIR="playground"

# install meta package containing all the tools we need
# including git and build-base.
# build base itself installs:
# - binutils
# - fortify-headers
# - g++
# - gcc
# - libc-dev
# - make
# we further install shadow for user management since we
# dont want to build the kernel as root
RUN apk add --update \
    alpine-sdk \
    shadow

# create our user who will be used to build the actual kernel
RUN useradd -m ${USERNAME}
USER ${USERNAME}

# fetch the linux kernel!
RUN mkdir -p /home/${USERNAME}/linux/build && \
    git clone --depth 1 https://www.github.com/torvalds/linux.git/ /home/${USERNAME}/linux/src
# use the parent directory of the linux kernel as our default directory
WORKDIR /home/${USERNAME}/linux
